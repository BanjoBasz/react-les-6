import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import Games from "./Games";
import Boeken from "./Boeken";
import Muziek from "./Muziek";

const App = () => {
  return (
    <Router>
      <div>
        <h1> Mijn website </h1>
        <ul>
          <li>
            <Link to="/games">Games</Link>
          </li>
          <li>
            <Link to="/boeken">Boeken</Link>
          </li>
          <li>
            <Link to="/muziek">Muziek</Link>
          </li>
          <li>
            <Link to="/google">Google</Link>
          </li>
        </ul>
        <Route path="/games" exact component={Games} />
        <Route path="/boeken" component={Boeken} />
        <Route path="/muziek" component={Muziek} />
      </div>
    </Router>
  );
};

export default App;
