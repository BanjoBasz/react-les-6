import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Artiest from "./Artiest";
const Muziek = ({ match }) => {
  return (
    <div>
      <ol>
        <li>
          <Link to={`${match.url}/nothing-but-thieves`}>
            Nothing but Thiefs
          </Link>
        </li>
        <li>
          <Link to={`${match.url}/kaiser-chiefs`}>Kaiser Chiefs</Link>
        </li>
        <li>
          <Link to={`${match.url}/the-cure`}>The Cure</Link>
        </li>
      </ol>
      <Route exact path={`${match.path}/:artist_name`} component={Artiest} />
    </div>
  );
};

export default Muziek;
