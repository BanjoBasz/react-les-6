import React from "react";

const Artiest = ({ match }) => {
  return (
    <div>
      <h1>Je hebt gekozen voor: {match.params.artist_name}</h1>
    </div>
  );
};

export default Artiest;
